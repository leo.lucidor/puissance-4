import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.Region;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar.ButtonData ;
import javafx.scene.control.ButtonType ;
import java.util.List;
import java.util.Arrays;
import java.io.File;
import java.util.ArrayList;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Background;
import javafx.util.Duration;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;

public class Puissance4 extends Application {

    Scene scene;

    @Override
    public void init() {
    }

    @Override
    public void start(Stage stage) {
        BorderPane root = new BorderPane();
        this.scene = new Scene(root, 500, 500);
        this.afficherAnimationPiece(root);
        stage.setTitle("Page accueil");
        stage.setScene(scene);
        stage.show();
    }

    public void afficherAnimationPiece(BorderPane root){
        HBox hBoxPiece = new HBox();
        hBoxPiece.setSpacing(30);
        ImageView pieceIMG1 = new ImageView(new Image("file:./img/piece.png"));
        ImageView pieceIMG2 = new ImageView(new Image("file:./img/piece.png"));
        ImageView pieceIMG3 = new ImageView(new Image("file:./img/piece.png"));
        ImageView pieceIMG4 = new ImageView(new Image("file:./img/piece.png"));
        hBoxPiece.getChildren().addAll(pieceIMG1, pieceIMG2, pieceIMG3, pieceIMG4);
        pieceIMG1.setFitHeight(60);
        pieceIMG1.setFitWidth(60);
        pieceIMG2.setFitHeight(60);
        pieceIMG2.setFitWidth(60);
        pieceIMG3.setFitHeight(60);
        pieceIMG3.setFitWidth(60);
        pieceIMG4.setFitHeight(60);
        pieceIMG4.setFitWidth(60);
        root.setBottom(hBoxPiece);
       
        // piece 1 animation
        RotateTransition rotation1 = new RotateTransition(Duration.seconds(1), pieceIMG1);
        rotation1.setByAngle(360);
        rotation1.setCycleCount(Animation.INDEFINITE);
        rotation1.setInterpolator(Interpolator.LINEAR);
        rotation1.play();
        Timeline gameLoop1 = new Timeline(new KeyFrame(Duration.millis(16), event -> {
            // Vérifier si l'image sort du stage
            if (pieceIMG1.getBoundsInParent().getMaxX() < 0) {
                // Réinitialiser la position de l'image à droite de l'écran
                pieceIMG1.setLayoutX(this.scene.getWidth());
            }
        }));
        gameLoop1.setCycleCount(Timeline.INDEFINITE);
        gameLoop1.play();
        TranslateTransition translation1 = new TranslateTransition(Duration.seconds(10), pieceIMG1);
        translation1.setByX(scene.getWidth());
        translation1.setCycleCount(Animation.INDEFINITE);
        translation1.setInterpolator(Interpolator.LINEAR);
        translation1.play();

        // piece 2 animation
        RotateTransition rotation2 = new RotateTransition(Duration.seconds(1), pieceIMG2);
        rotation2.setByAngle(360);
        rotation2.setCycleCount(Animation.INDEFINITE);
        rotation2.setInterpolator(Interpolator.LINEAR);
        rotation2.play();
        Timeline gameLoop2 = new Timeline(new KeyFrame(Duration.millis(16), event -> {
            // Vérifier si l'image sort du stage
            if (pieceIMG2.getBoundsInParent().getMaxX() < 0) {
                // Réinitialiser la position de l'image à droite de l'écran
                pieceIMG2.setLayoutX(this.scene.getWidth());
            }
        }));
        gameLoop2.setCycleCount(Timeline.INDEFINITE);
        gameLoop2.play();
        TranslateTransition translation2 = new TranslateTransition(Duration.seconds(10), pieceIMG2);
        translation2.setByX(scene.getWidth());
        translation2.setCycleCount(Animation.INDEFINITE);
        translation2.setInterpolator(Interpolator.LINEAR);
        translation2.play();

        // piece 3 animation
        RotateTransition rotation3 = new RotateTransition(Duration.seconds(1), pieceIMG3);
        rotation3.setByAngle(360);
        rotation3.setCycleCount(Animation.INDEFINITE);
        rotation3.setInterpolator(Interpolator.LINEAR);
        rotation3.play();
        Timeline gameLoop3 = new Timeline(new KeyFrame(Duration.millis(16), event -> {
            // Vérifier si l'image sort du stage
            if (pieceIMG3.getBoundsInParent().getMaxX() < 0) {
                // Réinitialiser la position de l'image à droite de l'écran
                pieceIMG3.setLayoutX(this.scene.getWidth());
            }
        }));
        gameLoop3.setCycleCount(Timeline.INDEFINITE);
        gameLoop3.play();
        TranslateTransition translation3 = new TranslateTransition(Duration.seconds(10), pieceIMG3);
        translation3.setByX(scene.getWidth());
        translation3.setCycleCount(Animation.INDEFINITE);
        translation3.setInterpolator(Interpolator.LINEAR);
        translation3.play();

        // piece 4 animation
        RotateTransition rotation4 = new RotateTransition(Duration.seconds(1), pieceIMG4);
        rotation4.setByAngle(360);
        rotation4.setCycleCount(Animation.INDEFINITE);
        rotation4.setInterpolator(Interpolator.LINEAR);
        rotation4.play();
        Timeline gameLoop4 = new Timeline(new KeyFrame(Duration.millis(16), event -> {
            // Vérifier si l'image sort du stage
            if (pieceIMG4.getBoundsInParent().getMaxX() < 0) {
                // Réinitialiser la position de l'image à droite de l'écran
                pieceIMG4.setLayoutX(this.scene.getWidth());
            }
        }));
        gameLoop4.setCycleCount(Timeline.INDEFINITE);
        gameLoop4.play();
        TranslateTransition translation4 = new TranslateTransition(Duration.seconds(10), pieceIMG4);
        translation4.setByX(scene.getWidth());
        translation4.setCycleCount(Animation.INDEFINITE);
        translation4.setInterpolator(Interpolator.LINEAR);
        translation4.play();
    }

    public static void main(String[] args) {
        launch(args);
    }    
}